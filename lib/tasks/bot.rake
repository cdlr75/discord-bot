require 'discordrb'

namespace :bot do
    desc "Handle messages from discord"
    task :listen  => :environment do

        # Manage events
        bot_event = Discordrb::Bot.new token: ENV['DISCORD_BOT_TOKEN']
        bot_event.message(with_text: "Ping!") do |event|
            event.respond "Pong!"
        end
        at_exit { bot_event.stop }
        bot_event.run(true) # non blocking

        # Manage commands
        bot = Discordrb::Commands::CommandBot.new(
            token: ENV['DISCORD_BOT_TOKEN'],
            client_id: ENV['DISCORD_BOT_CLIENT_ID'],
            prefix: '!'
        )

        bot.command :help do |event|
            event.user.pm("Supported commands
!start - Get an hello message
!ready min max - start a new game
!n number - new attempt
!attempts - get the number of attempts
!quit - stop the current game")
        end

        bot.command :start do |event|
            event.user.pm("Hello #{event.user.username}! You are about to start a new game.
Ready ? → !ready min max
Need help ? → !help")
        end

        bot.command :ready do |event, min, max|
            user = "#{event.user.username}_#{event.user.id}"
            min = min.to_i || 0
            max = max.to_i <= min.to_i ? min + 100 : max.to_i
            target = rand(min .. max)
            game = MinMax.find_by(user: user)
            if game.nil?
                MinMax.create(user: user, attempts: 0, target: target)
            else
                game.update(attempts: 0, target: target)
            end
            event.user.pm("Great! I have chose an number bewteen #{min} and #{max}.
Can you find it ? → !n number")
        end

        bot.command :n do |event, number|
            number = number.to_i
            user = "#{event.user.username}_#{event.user.id}"
            game = MinMax.find_by(user: user)
            event.user.pm("Start a new party ? → !ready") && next if game.nil?
            game.update attempts: game.attempts + 1
            event.user.pm("It is more than #{number}.") if game.target > number
            event.user.pm("It is less than #{number}.") if game.target < number
            event.user.pm("Yes! It is #{number}.") if game.target == number
        end

        bot.command :attempts do |event|
            user = "#{event.user.username}_#{event.user.id}"
            game = MinMax.find_by(user: user)
            if game.nil?
                event.user.pm("No current game. Start a new one ? !ready")
            else
                event.user.pm("Current game attempts: #{game.attempts}")
            end
        end

        bot.command :quit do |event|
            user = "#{event.user.username}_#{event.user.id}"
            MinMax.find_by(user: user)&.delete
            event.user.pm("Party is over!")
        end

        at_exit { bot.stop } # Gracefull shutdown
        bot.run
    end
end
