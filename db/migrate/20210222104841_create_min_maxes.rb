class CreateMinMaxes < ActiveRecord::Migration[6.1]
  def change
    create_table :min_maxes do |t|
      t.string :user
      t.integer :attempts
      t.integer :target

      t.timestamps
    end
  end
end
