require "test_helper"

class MinMaxesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @min_max = min_maxes(:one)
  end

  test "should get index" do
    get min_maxes_url
    assert_response :success
  end

  test "should get new" do
    get new_min_max_url
    assert_response :success
  end

  test "should create min_max" do
    assert_difference('MinMax.count') do
      post min_maxes_url, params: { min_max: { attempts: @min_max.attempts, target: @min_max.target, user: @min_max.user } }
    end

    assert_redirected_to min_max_url(MinMax.last)
  end

  test "should show min_max" do
    get min_max_url(@min_max)
    assert_response :success
  end

  test "should get edit" do
    get edit_min_max_url(@min_max)
    assert_response :success
  end

  test "should update min_max" do
    patch min_max_url(@min_max), params: { min_max: { attempts: @min_max.attempts, target: @min_max.target, user: @min_max.user } }
    assert_redirected_to min_max_url(@min_max)
  end

  test "should destroy min_max" do
    assert_difference('MinMax.count', -1) do
      delete min_max_url(@min_max)
    end

    assert_redirected_to min_maxes_url
  end
end
