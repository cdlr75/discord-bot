require "application_system_test_case"

class MinMaxesTest < ApplicationSystemTestCase
  setup do
    @min_max = min_maxes(:one)
  end

  test "visiting the index" do
    visit min_maxes_url
    assert_selector "h1", text: "Min Maxes"
  end

  test "creating a Min max" do
    visit min_maxes_url
    click_on "New Min Max"

    fill_in "Attempts", with: @min_max.attempts
    fill_in "Target", with: @min_max.target
    fill_in "User", with: @min_max.user
    click_on "Create Min max"

    assert_text "Min max was successfully created"
    click_on "Back"
  end

  test "updating a Min max" do
    visit min_maxes_url
    click_on "Edit", match: :first

    fill_in "Attempts", with: @min_max.attempts
    fill_in "Target", with: @min_max.target
    fill_in "User", with: @min_max.user
    click_on "Update Min max"

    assert_text "Min max was successfully updated"
    click_on "Back"
  end

  test "destroying a Min max" do
    visit min_maxes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Min max was successfully destroyed"
  end
end
