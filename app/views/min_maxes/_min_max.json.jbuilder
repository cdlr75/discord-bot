json.extract! min_max, :id, :user, :attempts, :target, :created_at, :updated_at
json.url min_max_url(min_max, format: :json)
