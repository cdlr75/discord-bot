class MinMaxesController < ApplicationController
  before_action :set_min_max, only: %i[ show edit update destroy ]

  # GET /min_maxes or /min_maxes.json
  def index
    @min_maxes = MinMax.all
  end

  # GET /min_maxes/1 or /min_maxes/1.json
  def show
  end

  # GET /min_maxes/new
  def new
    @min_max = MinMax.new
  end

  # GET /min_maxes/1/edit
  def edit
  end

  # POST /min_maxes or /min_maxes.json
  def create
    @min_max = MinMax.new(min_max_params)

    respond_to do |format|
      if @min_max.save
        format.html { redirect_to @min_max, notice: "Min max was successfully created." }
        format.json { render :show, status: :created, location: @min_max }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @min_max.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /min_maxes/1 or /min_maxes/1.json
  def update
    respond_to do |format|
      if @min_max.update(min_max_params)
        format.html { redirect_to @min_max, notice: "Min max was successfully updated." }
        format.json { render :show, status: :ok, location: @min_max }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @min_max.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /min_maxes/1 or /min_maxes/1.json
  def destroy
    @min_max.destroy
    respond_to do |format|
      format.html { redirect_to min_maxes_url, notice: "Min max was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_min_max
      @min_max = MinMax.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def min_max_params
      params.require(:min_max).permit(:user, :attempts, :target)
    end
end
