# Discord Bot + Rails

Sample application to process discord requests from a rails app.

## Features

#### Events

Bot reply to "Ping!" with "Pong!"

#### Commands

Bot can play "plus or minus" game with a user.

List of commands:
```
!start - Get an hello message
!ready min max - start a new game
!n number - new attempt
!attempts - get the number of attempts
!quit - stop the current game
```

Parties are persistent and a web app allow to supervise them (CRUD).

Start the stack and go to: http://0.0.0.0:3001/min_maxes

## Getting started

### Discord setup

- Create a personal server that will be the playground, "Test"
- Create a new discord app from https://discord.com/developers/applications
- Then, go to "Bot" menu and create a new bot for the app
- Then, go to "OAuth2" menu and select "bot" as scope and "administrator" as bot permission
- Copy pass the generated link to you browser to invite the bot to your server

You should now see the bot created as a new member of the discord server.
But it does nothing until we setup the Rails app.

### Rails setup

Build the docker image
```sh
docker build -t app --no-cache -f docker/Dockerfile .
```

Bring up the stack
```sh
SERVER_DB_CREATE=true SERVER_DB_SEED=true docker-compose -f docker/docker-compose.yml up
```

App should be up and running. App should respond to http://localhost:3001/

## Guidelines

This project is a playground focus on the [discordrb](https://github.com/shardlab/discordrb) integration in a Rails app.

Bot code: [lib/tasks/bot.rake](lib/tasks/bot.rake)
