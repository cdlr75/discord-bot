Rails.application.routes.draw do
  resources :min_maxes
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
